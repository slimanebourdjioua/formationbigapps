package com.bigapps.services
import com.bigapps.common.model._

import java.awt.color.ColorSpace


import org.apache.spark.sql.{Dataset, Encoders, SparkSession}
import org.junit.{After, Assert, Before, Test}

class TxServiceTest {

  val spark:SparkSession = SparkSession
    .builder
    .appName("Spark Pi")
    .master("local[*]")
    .getOrCreate()

  @Before
  def before(): Unit ={


  }


  @After
  def after(): Unit = {
    //  spark.close()
  }
  @Test
  def test_all_transactions_are_smaller_than_threshold()  {
    //given
    val transactionA = new Transaction(32,1000,54665,"debit",3,"30-09-2011")
    val transactionB = new Transaction(22,60,1236,"credit",3,"30-09-2010")
    import spark.implicits._
    val transactionDS:Dataset[Transaction] = spark.createDataset(Seq(transactionB))
    val threshold = 500.toDouble

    val expectedDS:Dataset[Transaction] =spark.createDataset(Seq(transactionB))

    //when
    val resultDS:Dataset[Transaction] = TxService.onlySmallTransactionsThanThreshold(transactionDS,threshold)
    //then
    Assert.assertEquals(expectedDS.collectAsList(),resultDS.collectAsList())
  }

  @Test
  def transactions_sum_by_client()=  {
    //given

    val transactionA = new Transaction(32,1000,54665,"debit",3,"30-09-2011")
    val transactionB = new Transaction(22,60,1236,"credit",3,"30-09-2010")
    val transactionC = new Transaction(2,100,5665,"credit",2,"30-09-2011")
    val transactionD = new Transaction(241,30,1236,"credit",2,"20-10-2017")

    val clientA = new  Client (2,"A","21-01-1945","33 rue de Meaux")
    val clientB = new  Client (3,"B","23-44-2003","10 avenue Victor Hugo")
    import spark.implicits._
    val transactionDS:Dataset[Transaction] = spark.createDataset(Seq(transactionA,transactionB,transactionC,transactionD))
    val clientDS: Dataset[Client]= spark.createDataset(Seq(clientA,clientB))

    val expectedDS : Dataset[(Client,Double)]=spark.createDataset(Seq((clientB,1060.toDouble),(clientA,130.toDouble)))

    //when
    val resultDS:Dataset[(Client,Double)] = TxService.sumTransactionsByClient(clientDS,transactionDS)

    //then
    assert(expectedDS.takeAsList(2).equals(resultDS.takeAsList(2)))
  }
  @Test
  def best_tansaction_for_each_client() = {
    //given
    val transactionA = new Transaction(32,1000,54665,"debit",3,"30-09-2011")
    val transactionB = new Transaction(22,60,1236,"credit",3,"30-09-2010")
    val transactionC = new Transaction(2,100,5665,"credit",2,"30-09-2011")
    val transactionD = new Transaction(241,30,1236,"credit",2,"20-10-2017")


    val clientA = new Client(2, "A", "21-01-1945", "33 rue de Meaux")
    val clientB = new Client(3, "B", "23-44-2003", "10 avenue Victor Hugo")
    import spark.implicits._
    val transactionDS:Dataset[Transaction] = spark.createDataset(Seq(transactionA,transactionB,transactionC,transactionD))
    val clientDS: Dataset[Client]= spark.createDataset(Seq(clientA, clientB))

    val expectedDS : Dataset[(Client,Transaction)] = spark.createDataset(Seq((clientB,transactionA),(clientA,transactionC)))

    //when
    val resultdDS : Dataset[(Client,Transaction)] = TxService.bestTansactionByClient(clientDS,transactionDS)
    //then
    assert(expectedDS.takeAsList(2).equals(resultdDS.takeAsList(2)))
  }
  @Test
  def average_Transactions_for_each_client(): Unit = {
    //given
    val transactionA = new Transaction(32, 1000, 54665, "debit", 3, "30-09-2011")
    val transactionB = new Transaction(22, 60, 1236, "credit", 3, "30-09-2010")
    val transactionC = new Transaction(2, 100, 5665, "credit", 2, "30-09-2011")
    val transactionD = new Transaction(241, 30, 1236, "credit", 2, "20-10-2017")

    val clientA = new Client(2, "A", "21-01-1945", "33 rue de Meaux")
    val clientB = new Client(3, "B", "23-44-2003", "10 avenue Victor Hugo")
    import spark.implicits._
    val transactionDS:Dataset[Transaction] = spark.createDataset(Seq(transactionA,transactionB,transactionC,transactionD))
    val clientDS: Dataset[Client]= spark.createDataset(Seq(clientA, clientB))

    val expectedDS : Dataset[(Client,Double)] = spark.createDataset(Seq((clientA,65.toDouble),(clientB,530.toDouble)))

    //when
    val resultdDS : Dataset[(Client,Double)] = TxService.avTransactionsByCient(clientDS,transactionDS)

    //then
    assert(expectedDS.takeAsList(2).equals(resultdDS.takeAsList(2)))

  }

  @Test
  def list_cliens_who_have_average_greater_than_value(){
    //given
    val transactionA = new Transaction(32, 100, 54665, "debit", 3, "30-09-2011")
    val transactionB = new Transaction(22, 60, 1236, "credit", 3, "30-09-2010")
    val transactionC = new Transaction(2, 100, 5665, "credit", 2, "30-09-2011")
    val transactionD = new Transaction(241, 30, 1236, "credit", 2, "20-10-2017")
    val transactionE = new Transaction(2, 200, 5665, "credit", 3, "30-09-2011")
    val transactionF = new Transaction(241, 20, 1236, "credit", 2, "20-10-2017")

    val clientA = new Client(2, "A", "21-01-1945", "33 rue de Meaux")
    val clientB = new Client(3, "B", "23-44-2003", "10 avenue Victor Hugo")

    val inPutValue = 100
    import spark.implicits._
    val transactionDS:Dataset[Transaction] = spark.createDataset(Seq(transactionA,transactionB,transactionC,transactionD,transactionE,transactionF))
    val clientDS: Dataset[Client]= spark.createDataset(Seq(clientA, clientB))

    val expectedDS : Dataset[(Client,Double)] = spark.createDataset(Seq((clientB,120.toDouble)))

    //when
    val resultdDS : Dataset[(Client,Double)] = TxService.listClientAvGreaterThen(clientDS,transactionDS,inPutValue)

    //then
    assert(expectedDS.takeAsList(1).equals(resultdDS.takeAsList(1)))

  }


  @Test
  def number_of_all_transactions() : Unit = {
    val transactionA = new Transaction(32, 1000, 54665, "debit", 3, "30-09-2011")
    val transactionB = new Transaction(22, 60, 1236, "credit", 3, "30-09-2010")
    val transactionC = new Transaction(2, 100, 5665, "credit", 2, "30-09-2011")
    val transactionD = new Transaction(241, 30, 1236, "credit", 2, "20-10-2017")
    val transactionE = new Transaction(2, 10, 5665, "credit", 3, "30-09-2011")
    val transactionF = new Transaction(241, 20, 1236, "credit", 2, "20-10-2017")
    import spark.implicits._
    val transactionDS:Dataset[Transaction] = spark.createDataset(Seq(transactionA,transactionB,transactionC,transactionD,transactionE,transactionF))

    val expectedCount = 6
    //when
    val resultCount =TxService.countTransactions(transactionDS)
    //then
    Assert.assertEquals(expectedCount,resultCount)
  }
  @Test
  def number_of_all_clients(): Unit ={

    val clientA = new Client(2, "A", "21-01-1945", "33 rue de Meaux")
    val clientB = new Client(3, "B", "23-44-2003", "10 avenue Victor Hugo")
    import spark.implicits._
    val clientDS: Dataset[Client]= spark.createDataset(Seq(clientA, clientB))
    val expectedCount = 2
    //when
    val resultCount =TxService.countClient(clientDS)
    //then
    Assert.assertEquals(expectedCount,resultCount)
  }

  @Test
  def average_of_lettest_three_transactions_for_each_client(): Unit ={
    //given
    val transactionA = new Transaction(32, 1000, 54665, "debit", 3, "10-09-2010")
    val transactionB = new Transaction(22, 60, 1236, "credit", 3, "10-09-2011")
    val transactionC = new Transaction(2, 100, 5665, "credit", 2, "11-09-2013")
    val transactionD = new Transaction(241, 30, 1236, "credit", 2, "10-10-2012")
    val transactionE = new Transaction(2, 10, 5665, "credit", 3, "10-09-2012")
    val transactionF = new Transaction(241, 20, 1236, "credit", 2, "10-10-2011")
    val transactionG = new Transaction(2, 50, 5665, "credit", 2, "10-09-2010")
    val transactionH = new Transaction(241, 70, 1236, "credit", 2, "10-10-2010")
    val transactionI = new Transaction(2, 80, 5665, "credit", 3, "10-09-2009")
    val transactionJ = new Transaction(241, 40, 1236, "credit", 2, "10-10-2009")

    val clientA = new Client(2, "A", "21-01-1945", "33 rue de Meaux")
    val clientB = new Client(3, "B", "23-44-2003", "10 avenue Victor Hugo")


    import spark.implicits._
    val transactionDS:Dataset[Transaction] = spark.createDataset(Seq(transactionA,transactionB,transactionC,transactionD,transactionE,transactionF,transactionG,transactionH,transactionI,transactionJ))
    val clientDS: Dataset[Client]= spark.createDataset(Seq(clientA, clientB))


    val expectedDS : Dataset[(Client,Double)] = spark.createDataset(Seq((clientB,1070.toDouble/3.toDouble),(clientA,50.toDouble)))

    //when
    val resultdDS : Dataset[(Client,Double)] = TxService.avLettestThreeTransactionsByClient(clientDS,transactionDS)

    //then

    expectedDS.foreach(d=>println(d))
    resultdDS.foreach(d=>println(d))



    assert(expectedDS.takeAsList(2).equals(resultdDS.takeAsList(2)))

  }

  @Test
  def dalta_of_transactions_for_each_client(): Unit ={
    //given
    val transactionA = new Transaction(32, 1000, 54665, "debit", 3, "30-09-2012")
    val transactionB = new Transaction(22, 60, 1236, "credit", 3, "30-09-2011")
    val transactionC = new Transaction(2, 100, 5665, "credit", 2, "30-09-2012")
    val transactionD = new Transaction(241, 30, 1236, "credit", 2, "30-09-2011")
    val transactionE = new Transaction(2, 10, 5665, "credit", 3, "30-09-2010")
    val transactionF = new Transaction(241, 20, 1236, "credit", 2, "20-09-2010")

    val clientA = new Client(2, "A", "21-01-1945", "33 rue de Meaux")
    val clientB = new Client(3, "B", "23-44-2003", "10 avenue Victor Hugo")
    import spark.implicits._
    val transactionDS:Dataset[Transaction] = spark.createDataset(Seq(transactionA,transactionB,transactionC,transactionD,transactionE,transactionF))
    val clientDS: Dataset[Client]= spark.createDataset(Seq(clientA, clientB))

    val sq=Seq((clientB,List(1000.toDouble,-940.toDouble,-50.toDouble)),(clientA,List(100.toDouble,-70.toDouble,-10.toDouble)))
    val expectedDS : Dataset[(Client,List[Double])] = spark.createDataset(sq)

    //when
    val resultdDS : Dataset[(Client,List[Double])] = TxService.daltaTransactionsByClient(clientDS,transactionDS)

    //then
    assert(expectedDS.takeAsList(2).equals(resultdDS.takeAsList(2)))



  }

  @Test
  def average_of_all_transactions ={
    //given
    val transactionA = new Transaction(32, 1000, 54665, "debit", 3, "30-09-2011")
    val transactionB = new Transaction(22, 60, 1236, "credit", 3, "30-09-2010")
    val transactionC = new Transaction(2, 100, 5665, "credit", 2, "30-09-2011")
    val transactionD = new Transaction(241, 30, 1236, "credit", 2, "20-10-2017")
    val transactionE = new Transaction(2, 10, 5665, "credit", 3, "30-09-2011")
    val transactionF = new Transaction(241, 20, 1236, "credit", 2, "20-10-2017")

    import spark.implicits._
    val transactionDS:Dataset[Transaction] = spark.createDataset(Seq(transactionA,transactionB,transactionC,transactionD,transactionE,transactionF))
    val expectedAvg =(1220.toDouble/6.toDouble).toDouble

    //when
    val resultAvg= TxService.avTransactions(transactionDS)
    //then

    assert(expectedAvg == resultAvg )


  }

}
