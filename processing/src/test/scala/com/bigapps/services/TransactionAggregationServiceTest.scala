/*package com.bigapps.services

import com.bigapps.common.models.Transaction
import org.apache.spark.sql.{Encoders, SparkSession}
import org.junit.{After, Before, Test
*/

/* class TransactionAggregationServiceTest {


 var spark:SparkSession = null
  @Before
  def before(): Unit ={
    spark = SparkSession
      .builder
      .appName("Spark Pi")
      .master("local[*]")
      .getOrCreate()
  }

  @Test
  def totalAmount(): Unit = {

    //Given
    val transactionA = new Transaction(32,1000,54665,"debit",3,"30-09-2011")
    val transactionB = new Transaction(22,1,1236,"credit",2,"20-10-2017")

    implicit val transactionEncoder = Encoders.product[Transaction]

    val expectedvalue = transactionA.amount + transactionB.amount

    val transactionDS = spark.createDataset(Seq(transactionA,transactionB))

    //When
    val totalAmount :Long  =  TransactionAggregationService.computeTotalAmount(transactionDS)

    //Then
    assert(totalAmount.equals(expectedvalue.asInstanceOf[Long]))
  }

  @Test
  def numberOfTransactions(): Unit = {
    //Given
    val transactionA = new Transaction(32,1000,54665,"debit",3,"30-09-2011")
    val transactionB = new Transaction(22,1,1236,"credit",2,"20-10-2017")

    implicit val transactionEncoder = Encoders.product[Transaction]

    val transactionDS = spark.createDataset(Seq(transactionA,transactionB))

    //When
    val numberOfTransactions :Long  =  TransactionAggregationService.getNumberOfTransactions(transactionDS)

    //Then
    assert(numberOfTransactions.equals(2.asInstanceOf[Long]))
  }


  @Test
  def allDebitTransactionsAmount(): Unit = {
    //Given
    val transactionA = new Transaction(32,1000,54665,"debit",3,"30-09-2011")
    val transactionB = new Transaction(22,1,1236,"debit",2,"20-10-2017")

    implicit val transactionEncoder = Encoders.product[Transaction]

    val transactionDS = spark.createDataset(Seq(transactionA,transactionB))

    //When
    val numberOfTransactions :Long  =  TransactionAggregationService.computeAmountOfAllDebitTransactions(transactionDS)

    //Then
    assert(numberOfTransactions.equals(1001.asInstanceOf[Long]))
  }

  @Test
  def allCreditTransactionsAmount(): Unit = {
    //Given
    val transactionA = new Transaction(32,1000,54665,"credit",3,"30-09-2011")
    val transactionB = new Transaction(22,1,1236,"credit",2,"20-10-2017")

    implicit val transactionEncoder = Encoders.product[Transaction]

    val transactionDS = spark.createDataset(Seq(transactionA,transactionB))

    //When
    val numberOfTransactions :Long  =  TransactionAggregationService.computeAmountOfAllCreditTransactions(transactionDS)

    //Then
    assert(numberOfTransactions.equals(1001.asInstanceOf[Long]))
  }

  @Test
  def bestDayOfTransactions(): Unit = {

    //Given

    val transactionA = new Transaction(32,1000,54665,"debit",3,"30-09-2011")
    val transactionB = new Transaction(22,1,1236,"credit",3,"30-09-2010")
    val transactionC = new Transaction(43,1000,54665,"debit",4,"30-09-2010")
    val transactionD = new Transaction(103,1,1236,"credit",3,"20-10-2017")

    val transactionE = new Transaction(32,1000,54665,"debit",3,"30-09-2011")
    val transactionF = new Transaction(22,1,1236,"credit",3,"30-09-2010")
    val transactionG = new Transaction(43,1000,54665,"debit",4,"30-09-2010")
    val transactionH = new Transaction(103,1,1236,"credit",3,"20-10-2017")


    implicit val transactionEncoder = Encoders.product[Transaction]

    val transactionDS = spark.createDataset(Seq(transactionA,transactionB,transactionC,transactionD,transactionE,transactionF,transactionG,transactionH))

    //When
    val bestDay :String =  TransactionAggregationService.getTheBestDayOfTransactions(transactionDS)

    //Then
    assert(bestDay.equals("30-09-2010"))
  }


  @After
  def after(): Unit = {
    spark.close()
  }

}*/
