package com.bigapps.services

import com.bigapps.processing.Transactions
import org.apache.spark.sql.SparkSession
import org.junit.{Assert, Test}

class JsonSerializeServicerTest {



 ////implicit val m = Manifest[Transactions]*
  @Test
  def deserializer_when_json_is_ok(){
    ///Given

    val myString = "{\"transactionId\":62,\"amount\":764,\"accountId\":16976047,\"typeDebitOrCredit\":\"credit\",\"clientId\":28,\"transactionDate\":\"26-10-1994\"}"
    val expectedTransaction = new Transactions(62,764,16976047,"credit",28,"26-10-1994")

    // When


    val result:Option[Transactions] = JsonSerializerService.fromJson(myString,classOf[Transactions])



    //Then
    Assert.assertEquals(expectedTransaction, result.get)

  }
  @Test
  def deserializer_when_json_is_nok(){
    ///Given

    val myString = "balablaaa"

    // When


    val result:Option[Transactions] = JsonSerializerService.fromJson(myString,classOf[Transactions])



    //Then
    Assert.assertTrue(result.isEmpty)

  }


  @Test
  def verify_createDataFrame_method{

    val spark: SparkSession = SparkSession.builder.master("local").getOrCreate
    val sc = spark.sparkContext // Just used to create test RDDs

    ///Given
    val transactions = List(new Transactions(62,764,16976047,"credit",28,"26-10-1994"), new Transactions(62,764,16976047,"credit",28,"26-10-1994"))

    val rdd = sc.parallelize(transactions.toSeq )


    // When


    val df = spark.createDataFrame(rdd)
    df.printSchema()
    df.show()


    //Then
    Assert.assertEquals(df.collectAsList,transactions)

  }

}