
package com.bigapps.services


import com.bigapps.common.model._
import com.bigapps.services._
import org.apache
import org.apache._
import org.apache.spark.sql.{Encoders, SparkSession}
import org.junit._
import com.bigapps.common.model._
import com.bigapps.common.models.ClientsTransactions
import org.apache.spark.sql.{Encoders, SparkSession}


class ClientAggregationServiceTest {

  implicit var spark: SparkSession = null
  implicit val clientEncoder = Encoders.product[Client]
  implicit val clientUpperEncoder = Encoders.product[ClientUpper]
  implicit val transactionEncoder = Encoders.product[Transaction]


  @Before
  def before() = {

    spark = SparkSession
      .builder
      .appName("Spark Pi")
      .master("local[*]")
      .getOrCreate()
  }


  @Test
  def UdfClient() = {

    //Given

    val clientLower = new Client(2, "aaa", "23-44-2003", "10 avenue Victor Hugo")
    val clientUpeer = new ClientUpper(2, "aaa", "23-44-2003", "10 avenue Victor Hugo", "AAA")

    val clientUpperDS = spark.createDataset(Seq(clientUpeer))

    val clientDS = spark.createDataset(Seq(clientLower))


    //When

    val udfClients = ClientAggregationService.ToUpperCAse(clientDS)

    //Then

    assert(clientUpperDS.takeAsList(1).equals((udfClients.takeAsList(1))))

  }


  @Test
  def bestClientWithBestOfTransaction() = {

    //Given

    val clientA = new Client(2, "A", "23-44-2003", "10 avenue Victor Hugo")
    val clientB = new Client(3, "B", "13-44-2003", "10 rue Victor Hugo")


    val transactionA = new Transaction(32, 1000, 54665, "credit", 3, "30-09-2011")
    val transactionB = new Transaction(22, 1, 1236, "credit", 2, "20-10-2017")
    val transactionC = new Transaction(2, 100, 5665, "credit", 2, "30-09-2011")
    val transactionD = new Transaction(241, 1, 1236, "credit", 2, "20-10-2017")


    val expectedClient = new ClientWithNumberOfTransaction(2, "A", "23-44-2003", "10 avenue Victor Hugo", 3)


    val clientDS = spark.createDataset(Seq(clientA, clientB))
    val transactionDS = spark.createDataset((Seq(transactionA, transactionB, transactionC, transactionD)))

    //When


    val bestClientMadeTransaction: ClientWithNumberOfTransaction = ClientAggregationService.best_client_made_transaction(clientDS, transactionDS) /// collectlist


    //Then

    assert(expectedClient.equals((bestClientMadeTransaction)))

  }


  // bestClient

  @Test
  def bestClientWithBestAmountOfTransactionCredit() {

    //Given

    val clientA = new Client(2, "A", "21-01-1945", "33 rue de Meaux")
    val clientB = new Client(3, "B", "23-44-2003", "10 avenue Victor Hugo")

    val transactionA = new Transaction(32, 1000, 54665, "credit", 3, "30-09-2011")
    val transactionB = new Transaction(22, 1, 1236, "credit", 2, "20-10-2017")

    val expectedClient = clientB.copy()
    val expectedAmount = 1000.asInstanceOf[Long]


    val clientDS = spark.createDataset(Seq(clientA, clientB))
    val transactionDS = spark.createDataset(Seq(transactionA, transactionB))


    //When
    val (bestClient, amount): (Client, Long) = ClientAggregationService.best_client(clientDS, transactionDS)


    //Then
    assert((expectedClient.equals(bestClient)) && ((expectedAmount.equals(amount))))


  }

  ///

  @Test
  def bestDayfTransaction() = {

    //Given

    val transactionA = new Transaction(32, 1000, 54665, "credit", 3, "30-09-2011")
    val transactionB = new Transaction(22, 1, 1236, "credit", 2, "30-09-2011")
    val transactionC = new Transaction(2, 100, 5665, "credit", 2, "30-09-2011")
    val transactionD = new Transaction(241, 1, 1236, "credit", 2, "20-10-2017")

    val expectedDayTransaction = "30-09-2011"

    print(expectedDayTransaction)

    //val clientDS = spark.createDataset(Seq(clientA,clientB))
    val transactionDS = spark.createDataset((Seq(transactionA, transactionB, transactionC, transactionD)))

    //When

    val bestDayTransaction: String = ClientAggregationService.best_day_transaction(transactionDS) /// collectlist

    //Then

    assert(expectedDayTransaction.equals((bestDayTransaction)))

  }


  ///

  @Test
  def ageBestClient() = {

    //Given

    val clientA = new Client(2, "A", "08-04-1994", "33 rue de Meaux")


    val expectedAge = 24

    //When

    val age: Int = ClientAggregationService.ageClient(clientA.birthday) /// collectlist

    //Then

    assert(expectedAge.equals((age)))

  }

  ///


  @Test
  def youngest_client() = {

    //Given

    val clientA = new Client(2, "idris", "08-08-1994", "33 rue de Meaux")
    val clientB = new Client(3, "fouad", "30-01-1991", "33 rue de Meaux")
    val clientC = new Client(2, "idris", "08-08-1989", "33 rue de Meaux")
    val clientD = new Client(3, "fouad", "30-01-1990", "33 rue de Meaux")

     val expectedClient = new ClientNameAndAge("idris" , 24)


    val clientsDS = spark.createDataset(Seq(clientA, clientB, clientC, clientD))

    //When

    val youngestClient = ClientAggregationService.youngest_client(clientsDS) /// collectlist

    //Then

    assert(expectedClient.equals((youngestClient)))

  }


  /*  @Test
    def transactions_per_client(){

      //Given

      val clientA = new  Client (2,"A","21-01-1945","33 rue de Meaux")

      val transactionA = new Transaction(32,1000,54665,"credit",2,"30-09-2011")
      val transactionB = new Transaction(22,1,1236,"credit",2,"20-10-2017")
      val transactionC = new Transaction(32,1000,54665,"credit",2,"30-09-2011")
      val transactionD = new Transaction(22,1,1236,"credit",2,"20-10-2017")




      implicit val transactionEncoder = Encoders.product[Transaction]

      val transactionArray = new Array[Transaction](4)

      transactionArray(0) = transactionA
      transactionArray(1) = transactionB
      transactionArray(2) = transactionC
      transactionArray(3) = transactionD

      val transactionDS =spark.createDataset(transactionArray)

      val expectedClientTransaction = new ClientsTransactions(clientA.name,transactionArray)



      //When
      val transactionPerClient =  ClientAggregationService.transactions_per_client(transactionDS,clientA)


      //Then

      assert( (expectedClientTransaction.name.equals(transactionPerClient.name)))


      var x = 0
      for ( t <- expectedClientTransaction.transactions){

        assert(t.equals(transactionPerClient.transactions.apply(x)))

        x+=1
      }


    }*/


  /*
    @Test
    def totalAmount(): Unit = {

      val transactionA = new Transaction(32,1000,54665,"debit",3,"30-09-2011")
      val transactionB = new Transaction(22,1,1236,"credit",2,"20-10-2017")

     implicit val transactionEncoder = Encoders.product[Transaction]

      val expectedvalue = transactionA.amount + transactionB.amount

      val transactionDS = spark.createDataset(Seq(transactionA,transactionB))


     //When
      val totalAmount :Long  =  ClientAggregationService.totalAmount(transactionDS)  /// collectlist

      // .colect il ramene tous les données

      //Then
      assert(totalAmount.equals(expectedvalue.asInstanceOf[Long]))


  }
  */


  /*
      @Test
      def numbrOfTransactionPerClient(): Unit ={
        //Given
        val clientA = new  Client (2,"A","21-01-1945","33 rue de Meaux")
        val transactionA = new Transaction(32,1000,54665,"credit",3,"30-09-2011")
        val transactionB = new Transaction(22,15,1236,"credit",2,"20-10-2017")
        val transactionC = new Transaction(12,100,65,"credit",2,"30-09-2011")
        implicit val clientEncodres = Encoders.product[Client]
        implicit val transitionEncoders = Encoders.product[Transaction]
        val expectedClientNumberTransaction = clientA.copy()// we want bestClient  clientB
        //import spark.implicits._
        val clientDS = spark.createDataset(Seq(clientA))
        val transactionDS = spark.createDataset(Seq(transactionA,transactionB,transactionC))
        //When
        val bestClientTransaction: Client =  ClientAggregationService.bestClientMadeTransaction(clientDS,transactionDS)  /// collectlist
        //Then
        assert(expectedClientNumberTransaction.equals((bestClientTransaction)))
      }*/


}

