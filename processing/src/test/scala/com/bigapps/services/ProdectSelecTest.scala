package com.bigapps.services
import com.bigapps.common.model.{ClientProducts, LimitRule, Transaction}
import org.apache.spark.sql.{Dataset, SparkSession}
import org.junit.{After, Before, Test}

class ProdectSelecTest {

  val spark:SparkSession = SparkSession
    .builder
    .appName("Spark Pi")
    .master("local[*]")
    .getOrCreate()

  @Before
  def before(): Unit ={


  }


  @After
  def after(): Unit = {
    //  spark.close()
  }





  @Test
  def select_productsByRule ={
    //given

    val r1 : ClientProducts = ClientProducts(1,"p1","c1",10,1)
    val r4 : ClientProducts = ClientProducts(1,"p4","c1",4,2)
    val r3 : ClientProducts = ClientProducts(1,"p3","c1",3,3)
    val r2 : ClientProducts = ClientProducts(1,"p2","c1",2,0)

    val r5: ClientProducts = ClientProducts(1,"p1","c2",5,1)
    val r8 : ClientProducts = ClientProducts(1,"p4","c2",4,0)
    val r6: ClientProducts = ClientProducts(1,"p2","c2",2,0)
    val r7 : ClientProducts = ClientProducts(1,"p3","c2",3,0)

    val r12 : ClientProducts = ClientProducts(1,"p4","c3",4,1)
    val r11 : ClientProducts = ClientProducts(1,"p3","c3",3,0)
    val r10 : ClientProducts = ClientProducts(1,"p2","c3",2,0)
    val r9 : ClientProducts = ClientProducts(1,"p1","c3",1,0)

    val r16: ClientProducts = ClientProducts(2,"p4","c1",4,1)
    val r15: ClientProducts = ClientProducts(2,"p3","c1",3,2)
    val r14: ClientProducts = ClientProducts(2,"p2","c1",2,3)
    val r13: ClientProducts = ClientProducts(2,"p1","c1",1,0)

    val r20: ClientProducts = ClientProducts(2,"p4","c2",4,1)
    val r19: ClientProducts = ClientProducts(2,"p3","c2",3,0)
    val r18: ClientProducts = ClientProducts(2,"p2","c2",2,0)
    val r17: ClientProducts = ClientProducts(2,"p1","c2",1,0)

    val r24 : ClientProducts = ClientProducts(2,"p4","c3",4,1)
    val r23: ClientProducts = ClientProducts(2,"p3","c3",3,0)
    val r22: ClientProducts = ClientProducts(2,"p2","c3",2,0)
    val r21: ClientProducts = ClientProducts(2,"p1","c3",1,0)


    import spark.implicits._
    val clientProductsDs :Dataset[ClientProducts] = spark.createDataset(Seq(r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16,r17,r18,r19,r20,r21,r22,r23,r24))

    val  LimitRule : LimitRule = new LimitRule(5, Map[String,Int]("c1" -> 3,"c2" -> 1,"c3" -> 1))

    val expectedDS :Dataset[ClientProducts] = spark.createDataset(Seq(r1,r4,r3,r5,r12,r16,r15,r14,r20,r24))


    //when
    val resultdDS :Dataset[ClientProducts] = ProdectSelectionService.selectProductBayRule(clientProductsDs, LimitRule)


    //than
    assert(expectedDS.collectAsList().equals(resultdDS.collectAsList()))

  }

}
