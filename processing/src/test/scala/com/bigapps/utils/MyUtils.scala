package com.bigapps.utils

import java.util.{ArrayList => JArrayList, List => JList}

import cucumber.api.DataTable
import org.apache.spark.sql.types.{DataTypes, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row, SparkSession}

import scala.collection.JavaConversions._


  object MyUtils {

    def parse(data: DataTable)(implicit spark: SparkSession): DataFrame = {

      val structfields = data.raw()
        .get(0)
        .toList
        .map(elm => {

          val splitted = elm.split(":")
          val name = splitted(0)
          val dataType = splitted(1)

          (name, dataType) match {
            case (name, "String") => new StructField(name, DataTypes.StringType, true)
            case (name, "Int") => new StructField(name, DataTypes.IntegerType, true)
            case (name, "Double") => new StructField(name, DataTypes.DoubleType, true)
            case (name, "Boolean") => new StructField(name, DataTypes.BooleanType, true)
            case (name, "Binary") => new StructField(name, DataTypes.BinaryType, true)
            case (name, "Float") => new StructField(name, DataTypes.FloatType, true)
            case (name, "Long") => new StructField(name, DataTypes.LongType, true)
            case (name, _) => new StructField(name, DataTypes.StringType, true)
          }
        })

      val sctrucType = StructType(structfields)

      val lignes = data.raw()

      val rows: List[Row] = lignes.subList(1, lignes.size())
        .map((rowAsString: JList[String]) => {

          var index = 0
          val listOfTypedObjects: JList[Any] = new JArrayList[Any]()

          rowAsString.toList.foreach((element: String) => {
            structfields(index).dataType match {

              case DataTypes.BooleanType => listOfTypedObjects.add(element.toBoolean)
              case DataTypes.IntegerType => listOfTypedObjects.add(element.toInt)
              case DataTypes.DoubleType => listOfTypedObjects.add(element.toDouble)
              case DataTypes.FloatType => listOfTypedObjects.add(element.toFloat)
              case DataTypes.StringType => listOfTypedObjects.add(element)
              case DataTypes.LongType => listOfTypedObjects.add(element.toLong)
              case _ => listOfTypedObjects.add(element.toString)


            }
            index += 1
          })

          Row.fromSeq(listOfTypedObjects.toSeq)
        }).toList

      val df = spark.createDataFrame(rows, sctrucType)
      df
    }


  }

