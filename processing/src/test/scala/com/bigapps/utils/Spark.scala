package com.bigapps.utils

import org.apache.spark.sql.SparkSession

object Spark {


  implicit val spark = SparkSession
    .builder
    .appName("Spark Pi")
    .master("local[*]")
    .getOrCreate()

}
