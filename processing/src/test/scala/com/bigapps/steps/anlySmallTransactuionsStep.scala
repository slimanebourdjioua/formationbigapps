package com.bigapps.steps

import com.bigapps.common.model._
import com.bigapps.utils.MyUtils


import com.bigapps.services.{ClientAggregationService, TxService}
import cucumber.api.DataTable
import cucumber.api.scala.{EN, ScalaDsl}
import org.apache.spark.sql._
import org.scalatest.Matchers

class anlySmallTransactuionsStep  extends EN with ScalaDsl with Matchers {

 val spark : SparkSession = SparkSession
    .builder
    .appName("Spark Pi")
    .master("local[*]")
    .getOrCreate()

  var txDS : Dataset[Transaction] = _
  var result :  Dataset[Transaction] = _
  var smalletthen : Double =_
  val encoder = org.apache.spark.sql.Encoders.product[Transaction]

  Given("""^the transactions$""") { (dataTable: DataTable) =>{

    import spark.implicits._
    txDS = MyUtils.parse(dataTable)(spark).as[Transaction]



  }
  }

  When("""^I call the onlySmallTransaction method$""") { () =>
    result = TxService.onlySmallTransactionsThanThreshold(txDS,100)

  }


  Then("""^the output must be$""") {
    { (dataTable: DataTable) =>
      val x = MyUtils.parse(dataTable)(spark).as(encoder)
      assert(x.collectAsList().equals(result.collectAsList()))
    }
  }

}
