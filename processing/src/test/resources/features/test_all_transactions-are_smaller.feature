Feature: Txservice

  Scenario:  test_all_transactions_are_smaller

    Given the transactions

      | transactionId:Int | amount:Int| accountId:Int| typeDebitOrCredit:String| clientId:Int | transactionDate:String|
      | 32  | 1000   | 54665     | debit             | 3        | 30-09-2012      |
      | 22  | 60     | 1236      | credit            | 3        | 30-09-2011      |
      | 2   | 100    | 5665      | credit            | 2        | 30-09-2012      |
      | 241 | 30     | 1236      | credit            | 2        | 30-09-2011      |
      | 2   | 10     | 5665      | credit            | 3        | 30-09-2010      |
      | 241 | 20     | 1236      | credit            | 2        | 20-09-2010      |





    When I call the onlySmallTransaction method

    Then  the output must be

      | transactionId:Int | amount:Int| accountId:Int| typeDebitOrCredit:String| clientId:Int | transactionDate: String|
      |            2|   100|     5665|           credit|       2|     30-09-2012|
      |          241|    30|     1236|           credit|       2|     30-09-2011|
      |          241|    20|     1236|           credit|       2|     20-09-2010|
      |           22|    60|     1236|           credit|       3|     30-09-2011|
      |            2|    10|     5665|           credit|       3|     30-09-2010|
