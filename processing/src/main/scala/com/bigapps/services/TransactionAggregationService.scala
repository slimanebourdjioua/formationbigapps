/*package com.bigapps.services

import com.bigapps.common.models.Transaction
import org.apache.spark.sql.{Dataset, SparkSession, functions}*/

/*object TransactionAggregationService {

  def computeAmountOfAllDebitTransactions(transactionDS: Dataset[Transaction]): Long ={
    val classifiedTransaction = transactionDS
      .groupBy("typeDebitOrCredit")
      .agg(functions.sum("amount").as("Total"))

    classifiedTransaction
      .where(classifiedTransaction("typeDebitOrCredit") === "debit")
      .head()
      .getLong(1)
  }

  def computeAmountOfAllCreditTransactions(transactionDS: Dataset[Transaction]): Long ={
    val classifiedTransaction = transactionDS
      .groupBy("typeDebitOrCredit")
      .agg(functions.sum("amount").as("Total"))

    classifiedTransaction
      .where(classifiedTransaction("typeDebitOrCredit") === "credit")
      .head()
      .getLong(1)
  }

  def computeTotalAmount(transactionDS: Dataset[Transaction]) : Long ={
    transactionDS
      .agg(functions.sum("amount"))
      .head()
      .getLong(0)
  }

  def getNumberOfTransactions(transactionDS: Dataset[Transaction]) : Long ={
    transactionDS
      .agg(functions.count("amount"))
      .head()
      .getLong(0)
  }

  def getTheBestDayOfTransactions(transactionDS: Dataset[Transaction]): String ={

    transactionDS
      .groupBy("transactionDate")
      .agg(functions.count("transactionDate").as("nb"))
      .orderBy(functions.desc("nb"))
      .takeAsList(1)
      .get(0)
      .getString(0)
  }*/

//}
