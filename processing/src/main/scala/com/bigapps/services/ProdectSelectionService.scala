package com.bigapps.services

import com.bigapps.common.model.{ClientProducts, LimitRule}
import org.apache.spark.sql.{Dataset, SparkSession}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._

object ProdectSelectionService {

  implicit val spark = SparkSession
    .builder
    .appName("Spark Pi")
    .master("local[*]")
    .getOrCreate()



  def selectProductBayRule(clientProductsDs: Dataset[ClientProducts], limitRule:LimitRule) : Dataset[ClientProducts] = {
    import spark.implicits._

    val byWeightOrder =
      Window.partitionBy("clientId","category").orderBy(desc("weight"))



    clientProductsDs
      .withColumn("rank", rank().over(byWeightOrder))
      .as[ClientProducts]
      .filter(clientProducts =>{

        clientProducts.rank <= limitRule.limitTotal
      })
      .filter(clientProducts =>{



        val currentCat = clientProducts.category

        val currentLimit = limitRule.limitByCategory.getOrElse(currentCat, 0)

        val currentRank = clientProducts.rank

        currentRank <= currentLimit

      })
      .orderBy("clientId","category")

      .as[ClientProducts]


  }





}
