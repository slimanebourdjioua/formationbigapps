package com.bigapps.services

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Date

import org.apache.spark.sql.functions._
import com.bigapps.common.model._
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.{Dataset, Encoders, Row, SparkSession}
import org.joda.time.DateTime

object TxService {


  implicit val spark = SparkSession
    .builder
    .appName("Spark Pi")
    .master("local[*]")
    .getOrCreate()
  println("Hello World!")

  import spark.implicits._


  def onlySmallTransactionsThanThreshold(txDS: Dataset[Transaction], amount: Double): Dataset[Transaction] = {

    txDS
      .filter(t => t.amount <= amount)
      .orderBy($"clientId")
      .as[Transaction]

  }

  def sumTransactionsByClient(clientDS: Dataset[Client], txDS: Dataset[Transaction]): Dataset[(Client, Double)] = {

    return txDS.join(clientDS, "clientId")
      .groupBy("clientId", "name", "birthday", "address")
      .agg(Map("amount" -> "sum"))
      .map(row => {
        import spark.implicits._
        val client: Client = new Client(row.getAs[Int](0), row.getAs[String](1), row.getAs[String](2), row.getAs[String](3))
        val sum = row.getAs[Long](4)
        (client, sum.toDouble)
      })


  }


  def bestTansactionByClient(clientDS: Dataset[Client], txDS: Dataset[Transaction]): Dataset[(Client, Transaction)] = {

    val byamountOrder =
      Window.partitionBy("clientId").orderBy(desc("amount"))

    return txDS
      .join(clientDS, "clientId")

      .withColumn("rank", rank().over(byamountOrder)).filter("rank == 1")

      .map(row => {
        val client: Client = new Client(row.getAs[Int](0), row.getAs[String](6), row.getAs[String](7), row.getAs[String](8))
        val transaction: Transaction = Transaction(row.getAs[Int](1), row.getAs[Int](2), row.getAs[Int](3), row.getAs[String](4), row.getAs[Int](0), row.getAs[String](5))
        (client, transaction)
      })


  }

  def avTransactionsByCient(clientDS: Dataset[Client], txDS: Dataset[Transaction]): Dataset[(Client, Double)] = {
    txDS.join(clientDS, "clientId")
      .groupBy("clientId", "name", "birthday", "address")
      .agg(Map("amount" -> "avg"))
      .orderBy(asc("clientId"))
      .map(row => {
        import spark.implicits._
        val client: Client = new Client(row.getAs[Int](0), row.getAs[String](1), row.getAs[String](2), row.getAs[String](3))
        val avg = row.getAs[Double](4)
        (client, avg.toDouble)

      })

  }

  def listClientAvGreaterThen(clientDS: Dataset[Client], txDS: Dataset[Transaction], value: Double): Dataset[(Client, Double)] = {

    txDS.join(clientDS, "clientId")

      .groupBy("clientId", "name", "birthday", "address")

      .agg(avg($"amount").as("av"))
      .filter(rw => {
        rw.getAs[Double]("av") >= value

      })
      .map(rw => {
        val client: Client = new Client(rw.getAs[Int](0), rw.getAs[String](1), rw.getAs[String](2), rw.getAs[String](3))
        val avg: Double = rw.getAs[Double]("av")
        (client, avg)
      })


  }

  def countTransactions(txDS: Dataset[Transaction]): Long = {
    return txDS.count()

  }

  def avTransactions(txDS: Dataset[Transaction]): Double = {
    return txDS
      .agg(avg($"amount").as("av"))
      .map(rw => rw.getAs[Double]("av"))
      .first().toDouble


  }

  def countClient(clientDS: Dataset[Client]): Long = {
    return clientDS.count()

  }


  def avLettestThreeTransactionsByClient(clientDS: Dataset[Client], txDS: Dataset[Transaction]): Dataset[(Client, Double)] = {
    import spark.implicits._

    val pattern = "dd-MM-yyyy"

    val byDateOrder =
      Window.partitionBy("clientId").orderBy(desc("date"))
    val byAvgOrder =
      Window.partitionBy("clientId").orderBy(desc("avg"))

    return txDS
      .join(clientDS, "clientId")
      .withColumn(
        "date",
        to_date(col("transactionDate"),pattern))


      .withColumn("rank", rank().over(byDateOrder))
      .filter("rank <= 3")
      .withColumn("avg", avg("amount") over  (byDateOrder.rowsBetween(-3, 3)))



      .filter("rank == 1")


      .map(row => {
        val client: Client = new Client(row.getAs[Int](0), row.getAs[String](6), row.getAs[String](7), row.getAs[String](8))

        (client, row.getAs[Double]("avg"))
      })


  }

  def daltaTransactionsByClient(clientDS: Dataset[Client], txDS: Dataset[Transaction]): Dataset[(Client, List[Double])] = {
    val pattern = "dd-MM-yyyy"
    import spark.implicits._

    val byDateOrder =
      Window.partitionBy("clientId").orderBy(desc("date"))
    val byrankOrder =
      Window.partitionBy("clientId").orderBy(desc("daltalist"))

    return txDS
      .join(clientDS, "clientId")
      .withColumn(
        "date",
        to_date(col("transactionDate"), pattern))


      .withColumn("lag", lag("amount", 1).over(byDateOrder))

      .withColumn("dalta", col("amount") - when(col("lag").isNotNull, col("lag")).otherwise(0.toDouble))
      .withColumn("rank", rank().over(byDateOrder.orderBy(asc("date"))))

      .withColumn("daltalist", collect_list("dalta") over (byDateOrder))

      .filter("rank =1")


      .map(row => {
        val client: Client = new Client(row.getAs[Int](0), row.getAs[String](6), row.getAs[String](7), row.getAs[String](8))

        (client, row.getAs[Seq[Double]]("daltalist"))
      }).as[(Client, List[Double])]

  }

}
