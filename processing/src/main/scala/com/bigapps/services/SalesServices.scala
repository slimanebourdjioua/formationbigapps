package com.bigapps.services

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{count, sum, udf}

object SalesServices {


  def toLimitFloat(fl: Float): Float = {

    BigDecimal(fl).setScale(2, BigDecimal.RoundingMode.DOWN).toFloat
  }





  def salesSum(productDF: DataFrame, salesDF: DataFrame): DataFrame = {

    val salesProductJoin = salesDF
      .join(productDF, productDF("productId") === salesDF("productId"))
      .drop(salesDF("productId"))
      .groupBy("productId")
      .agg(sum(("price")).as("sumOfSales"))
      .orderBy(("productId"))


    val x= salesProductJoin


  val limiteFloat : Float => Float = toLimitFloat(_)
   val limiteFloatUDF = udf(limiteFloat)

    x.withColumn("sumOfSales" , limiteFloatUDF(x.col("sumOfSales")))



  //  x.show()
  }


  def salesCount(productDF: DataFrame, salesDF: DataFrame): DataFrame = {


    val salesProductJoin = salesDF
      .join(productDF, productDF("productId") === salesDF("productId"))
      .drop(salesDF("productId"))
      .groupBy("productId")
      .agg(count("productId").as("numberOfSales"))
      .orderBy(("productId"))




     salesProductJoin

  }


}
