package com.bigapps.services

import com.bigapps.processing.Transactions
import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper

import scala.util.Try


object JsonSerializerService {


  val mapper = new ObjectMapper with ScalaObjectMapper
  mapper.registerModule(DefaultScalaModule)
  mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)


  def fromJson(json:String,clazz: Class[Transactions]):Option[Transactions] = {
    Try {
      val x =Some(mapper.readValue(json, clazz))
      x
    }.getOrElse(None)
  }


}
