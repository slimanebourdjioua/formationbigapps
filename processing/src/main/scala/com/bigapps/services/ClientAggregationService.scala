package com.bigapps.services
import org.apache.spark.sql.functions._
import java.time.LocalDate

import com.bigapps.common.model._
import org.apache.spark.sql._


object ClientAggregationService {


  def ToUpperCAse(clientsDS: Dataset[Client])(implicit spark: SparkSession): Dataset[ClientUpper] = {
    implicit val clientEncoder = Encoders.product[Client]
    implicit val clientUpperEncoder = Encoders.product[ClientUpper]
    val upper: String => String = _.toUpperCase
    val upperUDF = udf(upper)

    clientsDS
      .withColumn("upper", upperUDF(clientsDS.col("name")))
      .as[ClientUpper]
  }


  def
  ageClient(birthday: String): Int = {

    val brithDaySplit = birthday.split("-")


    LocalDate.of(brithDaySplit(2).toInt
      , brithDaySplit(1).toInt
      , brithDaySplit(0).toInt)
      .until(LocalDate.now).getYears

  }


  ////////////
  def best_day_transaction(transactions: Dataset[Transaction]): String = {


    val day = transactions
      .groupBy("transactiondate")
      .agg(count("transactiondate").as("date"))
      .orderBy(desc("date"))
      .limit(1)
      .takeAsList(1)
      .get(0)
      .get(0)
      .asInstanceOf[String]
    //day = day.collect()(0)(0).asInstanceOf[Long]

    day
  }


  /////////////////////
  def best_client(clients: Dataset[Client], transactions: Dataset[Transaction])(implicit spark: SparkSession): (Client, Long) = {

    import spark.implicits._

    // Filtre clientCreditTransaction
    val creditFilterTransaction = transactions
      .filter("typeDebitOrCredit  == 'credit'")
      .groupBy("clientId")
      .agg(sum(("amount")))

    // JOIN + SORT
    val clientTransactionJoin = creditFilterTransaction
      .join(clients, clients("clientID") === creditFilterTransaction("clientID"))
      .orderBy(desc("sum(amount)"))
      .drop((creditFilterTransaction("clientId")))


    val amount = clientTransactionJoin.collect()(0)(0).asInstanceOf[Long]

    val bestClient = clientTransactionJoin
      .limit(1)
      .as[Client]
      .takeAsList(1).get(0)

    (bestClient, amount)
  }

  ////
  def best_client_made_transaction(clientDS: Dataset[Client], transactionDS: Dataset[Transaction])(implicit spark: SparkSession): ClientWithNumberOfTransaction = {


    import spark.implicits._

    //JOIN + SORT
    val clientTransactionJoin = transactionDS
      .join(clientDS, clientDS("clientId") === transactionDS("clientId"))
      .drop(transactionDS("clientId"))
      .groupBy("clientId", "name", "birthday", "address")
      .agg(count("clientId").as("numberOfTransaction"))
      .orderBy(desc("numberOfTransaction"))

    //clientTransactionJoin.show()

    clientTransactionJoin
      .limit(1)
      .as[ClientWithNumberOfTransaction]
      .takeAsList(1).get(0)

  }

  ////  Oldest

  def oldest_client(clientDS: Dataset[Client])(implicit spark: SparkSession): ClientNameAndAge = {

    compute_client_ages(clientDS)
      .orderBy(desc("age"))
      .limit(1)
      .takeAsList(1)
      .get(0)
  }

  ////// .


  def youngest_client(clientDS: Dataset[Client])(implicit spark: SparkSession): ClientNameAndAge = {

    compute_client_ages(clientDS)
      .orderBy(asc("age"))
      .limit(1)
      .takeAsList(1)
      .get(0)
  }


  def compute_client_ages(clientDS: Dataset[Client])(implicit spark: SparkSession): Dataset[ClientNameAndAge] = {
    import spark.implicits._
    clientDS.map(client => new ClientNameAndAge (client.name,ageClient(client.birthday)))



  }
}
