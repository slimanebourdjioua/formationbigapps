package com.bigapps.processing

import java.util._

import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.kafka.clients.consumer.ConsumerRecord

import org.apache.kafka.clients.consumer.KafkaConsumer


object Consumer extends App {


  val props = new Properties()
  props.put("bootstrap.servers", "localhost:9092")
  props.put("enable.auto.commit", "true")
  props.put("group.id", "group")
  props.put("auto.commit.interval.ms", "1000")
  props.put("session.timeout.ms", "30000")
  props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
  props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")

  val topic = Arrays.asList("transactions")
  val consumer: KafkaConsumer[String, String] = new KafkaConsumer[String, String](props)

  consumer.subscribe(topic)


  while (true) {
    val rs: ConsumerRecords[String, String] = consumer.poll(100)
    val it = rs.iterator()
    while (it.hasNext)
      println(it.next().value())


  }


}





