/*
package com.bigapps.processing

/* Write to ES from Spark(scala) */
import org.apache.spark.{SparkConf, SparkContext}
import org.elasticsearch.spark.rdd.EsSpark
case class Trip(departure: String, arrival: String)

object WriteToES extends  App {

    val conf = new SparkConf()
    conf.set("spark.master", "local[*]")
    conf.set("spark.app.name", "testing")
    conf.set("spark.es.nodes", "localhost")
    conf.set("es.index.auto.create", "true")
    //  conf.set("spark.es.net.ssl","false")
    conf.set("spark.es.port", "9200")
    //  conf.set("es.mapping.date.rich","false")
    conf.set("spark.es.index.read.missing.as.empty", "true")
    //   conf.set("spark.es.batch.write.retry.count","-1")
    //  conf.set("spark.serializer","org.apache.spark.serializer.KryoSerializer")
    //  conf.set("es.nodes.discovery", "false")

    val sc = new SparkContext(conf)
    // define a case class
    val numbers = Map("un" -> 1, "deux" -> 2, "trois" -> 3)
    val airports = Map("arrival" -> "Otopeni", "SFO" -> "San Fran")

    val rdd = sc.makeRDD(Seq(numbers, airports))
    EsSpark.saveToEs(rdd, "fouad/ddd")


 ///   val esRdd = sc.esRDD("spark/docs")



}*/
