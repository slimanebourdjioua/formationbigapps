/*
package com.bigapps.processing
import com.bigapps.services.JsonSerializerService
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.KafkaUtils
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.elasticsearch.spark.rdd.EsSpark

object  SparkStreamingKafka extends App {


  val conf = new SparkConf()
    .setMaster("local[*]")
    .setAppName("SparkMe App")

  val spark = SparkSession.builder().config(conf ).getOrCreate()

  val sc = spark.sparkContext

  val kafkaParams = Map[String, Object](
    "bootstrap.servers" -> "localhost:9092",
    "key.deserializer" -> classOf[StringDeserializer],
    "value.deserializer" -> classOf[StringDeserializer],
    "group.id" -> "kafka example",
    "auto.offset.reset" -> "earliest",
    "enable.auto.commit" -> (false: java.lang.Boolean)
  )
  val streamingContext = new StreamingContext(sc, Seconds(10))
  val topic = Array("transactions")

  val stream = KafkaUtils.createDirectStream[String,  String](
    streamingContext,
    PreferConsistent,
    Subscribe[String, String](topic,kafkaParams)
  )



  val rdds = stream.map(r => r.value())
  rdds.foreachRDD { rdd =>
    /// rdd.collect().foreach(println)
    val rddTransactions =  rdd.map { record =>  JsonSerializerService.fromJson(record,classOf[Transactions]) }
      .filter{element: Option[Transactions] => element.isDefined}
      .map{element : Option[Transactions] => element.get}

    //spark.createDataFrame(rddTransactions)
    EsSpark.saveToEs(rddTransactions, "data/transactions")

    // createDataFrame(rddTransactions,Encoders.bean(classOf[Transactions]).schema)
  }



  streamingContext.start()
  streamingContext.awaitTermination()

}*/
