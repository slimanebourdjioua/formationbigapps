package com.bigapps.processing
import java.sql.Date
import java.util.Optional

import com.bigapps.common.model.{ClientProducts, LimitRule}
import org.apache.spark.sql.functions._
//import com.bigapps.utils.MyUtils
import com.bigapps.common.model.{Client, Transaction}
import cucumber.api.DataTable
import org.apache.spark.sql._
import org.apache.spark.sql.expressions.Window

object Test {


  def main(args: Array[String]): Unit = {

    implicit val spark = SparkSession
      .builder
      .appName("Spark Pi")
      .master("local[*]")
      .getOrCreate()

    import spark.implicits._


    /// Read fil client .

    val r1: ClientProducts = ClientProducts(1, "p1", "c1", 1,0)
    val r2: ClientProducts = ClientProducts(1, "p2", "c1", 2,0)
    val r3: ClientProducts = ClientProducts(1, "p3", "c1", 3,0)
    val r4: ClientProducts = ClientProducts(1, "p4", "c1", 4,0)


    val r5: ClientProducts = ClientProducts(1, "p1", "c2", 1,0)
    val r6: ClientProducts = ClientProducts(1, "p2", "c2", 2,0)
    val r7: ClientProducts = ClientProducts(1, "p3", "c2", 3,0)
    val r8: ClientProducts = ClientProducts(1, "p4", "c2", 4,0)

    val r9: ClientProducts = ClientProducts(1, "p1", "c3", 1,0)
    val r10: ClientProducts = ClientProducts(1, "p2", "c3", 2,0)
    val r11: ClientProducts = ClientProducts(1, "p3", "c3", 3,0)
    val r12: ClientProducts = ClientProducts(1, "p4", "c3", 4,0)

    val r13: ClientProducts = ClientProducts(2, "p1", "c1", 1,0)
    val r14: ClientProducts = ClientProducts(2, "p2", "c1", 2,0)
    val r15: ClientProducts = ClientProducts(2, "p3", "c1", 3,0)
    val r16: ClientProducts = ClientProducts(2, "p4", "c1", 4,0)

    val r17: ClientProducts = ClientProducts(2, "p1", "c2", 1,0)
    val r18: ClientProducts = ClientProducts(2, "p2", "c2", 2,0)
    val r19: ClientProducts = ClientProducts(2, "p3", "c2", 3,0)
    val r20: ClientProducts = ClientProducts(2, "p4", "c2", 4,0)

    val r21: ClientProducts = ClientProducts(2, "p1", "c3", 1,0)
    val r22: ClientProducts = ClientProducts(2, "p2", "c3", 2,0)
    val r23: ClientProducts = ClientProducts(2, "p3", "c3", 3,0)
    val r24: ClientProducts = ClientProducts(2, "p4", "c3", 4,0)


    import spark.implicits._
    val clientProductsDs: Dataset[ClientProducts] = spark.createDataset(Seq(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24))


    val m =  Map[String,Int]("c1" -> 3, "c2" -> 1, "c3" -> 1)
    val limitRule: LimitRule = LimitRule( 5 , m )
   // limitRule.check

    val expectedDS: Dataset[ClientProducts] = spark.createDataset(Seq(r2, r3, r4, r8, r12, r14, r15, r16, r20, r24))




    val byWeightOrder =
      Window.partitionBy("clientId","category").orderBy(desc("weight"))



    clientProductsDs
      .withColumn("rank", rank().over(byWeightOrder))
      .as[ClientProducts]
      .filter(clientProducts =>{

        clientProducts.rank <= limitRule.limitTotal
      })
      .filter(clientProducts =>{



        val currentCat = clientProducts.category

        val currentLimit = limitRule.limitByCategory.getOrElse(currentCat, 0)

        val currentRank = clientProducts.rank

        currentRank <= currentLimit

      })
   .orderBy("clientId","category")

      .as[ClientProducts]
      .show()


      /*
        .withColumn("rank", rank().over(bycategoryOrder.partitionBy("clientId","category")))
      .filter(rw =>{
        rw.getAs[Int]("rank") <= limitRule.limitTotal
      })
      .filter(rw =>{
        var boul : Boolean = false

        limitRule.limitByCategory.keySet.foreach(f=>{

       boul =   boul ||  f == rw.getAs[String]("category")  && rw.getAs[Int]("rank") <= limitRule.limitByCategory.get(f).get

        })
        boul

      })
     // .drop("rank")
      .orderBy("clientId","category").as[ClientProducts]

      .show()

*/
  }
}

