
package com.bigapps.processing

import java.util.Properties

import com.google.gson.Gson
import org.apache.kafka.clients.producer._

import scala.io.Source.fromFile

/**
  * Created by yury on 24.06.17.
  */
object KafkaProducer extends App {



  val props = new Properties()
  props.put("bootstrap.servers", "localhost:9092")
  props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

  val producer = new KafkaProducer[String, String](props)

  val TOPIC = "transactions"

  val gson = new Gson

  for (us <- readData("/home/bourdjioua/Documents/bigapps_masters/bigapps_stage/processing/src/main/resources/transactionnormale.csv")) {
    println(gson.toJson(us))
    val record = new ProducerRecord(TOPIC,"", gson.toJson(us))
    Thread.sleep(50)
    producer.send(record)
  }

  producer.close()

  private def readData(fileName: String): List[Transactions] = {
      fromFile(fileName).getLines().drop(1)
      .map(line => line.split(",").map(_.trim))
      .map(parts => Transactions(parts(0).toInt, parts(1).toInt, parts(2).toInt, parts(3), parts(4).toInt, parts(5)))
      .toList
  }
}
