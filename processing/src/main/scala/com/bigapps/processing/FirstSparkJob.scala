

package com.bigapps.processing
import com.bigapps.common.model._
import com.bigapps.services.ClientAggregationService
import org.apache.spark.sql.SparkSession



object FirstSparkJob {

  def main(args: Array[String]): Unit = {


    implicit val spark = SparkSession
      .builder
      .appName("Spark Pi")
      .master("local[*]")
      .getOrCreate()

    import spark.implicits._


    /// Read fil client .


    val clients = spark.read

      .option("header", true)
      .option("inferSchema", true)
      .csv("/home/bourdjioua/IdeaProjects/bigapps_masters/bigapps_stage/processing/src/main/resources/client.csv")
      .as[Client]


    val transactions = spark.read

      .option("header", true)
      .option("inferSchema", true)
      .csv("/home/bourdjioua/IdeaProjects/bigapps_masters/bigapps_stage/processing/src/main/resources/transactionnormale.csv")
      .as[Transaction]


    //
    val bestClientMadeTransaction: ClientWithNumberOfTransaction = ClientAggregationService.best_client_made_transaction(clients, transactions)
    //
    val (bestClient, amount): (Client, Long) = ClientAggregationService.best_client(clients, transactions)
    //
    val besDateOfTransaction: String = ClientAggregationService.best_day_transaction(transactions)
    //val ageBestClient : Int  = ClientAggregationService.ageClient(bestClient)
    val youngestClient = ClientAggregationService.youngest_client(clients)
    val oldestClient = ClientAggregationService.oldest_client(clients)
    val UdfClients = ClientAggregationService.ToUpperCAse(clients)

    //  val transactionsPerClients  = ClientAggregationService.transactions_per_clients(clients,transactions)

    //ClientAggregationService.transactions_per_clients(clients,transactions).show()
    // transactionsPerClient.collect().foreach(x => x.printTransactionName())
    // Show
    //println(s"our Best Client is ${bestClient.name} and his age is ${ageBestClient} years old with a nimbr of trasaction ${bestClientMadeTransaction.numberOfTransaction}")
    println(s" the best client who has the biggest credit amount is   ${amount}  ")
    println(s" the day when there is more transaction ${besDateOfTransaction}")
    println(s" the best client who made trasaction is ${bestClientMadeTransaction}")
    println(s"the youngest is ${youngestClient.name} with an age of" +
      s" ${youngestClient.age}and the oldest is ${oldestClient.name} with an age of ${oldestClient.age}")


  }


}

















/*






  //    import spark.implicits._

     val clientTransactionJoin = transactions
        .join(clients, clients("clientID") === transactions("clientID"))
        .orderBy(functions.desc("amount"))
        .drop((transactions("clientId")))
        .show()





    val clientsDS = clients.as[Client]
    val transactionDS = transactions.as[Transaction]

    val clientTransactionJoin = clientsDS
      .join(transactionDS, clientsDS("id") === transactionDS("clientid"))
     // .show(100)


    val totalAmount = transactionDS
      .agg(functions.sum("amount").as("totalAmount"))
      .show()


    val orderOfBestClientsByAmount = clientTransactionJoin
     .groupBy("clientid")
      .agg(functions.max("amount").as("MaxAmount"))
 //HEAD
      .orderBy(functions.desc("MaxAmount"))
      .show()

//=======
    .orderBy(functions.desc("MaxAmount"))
   // .show()*/




//aa977e04f7cdada9805dd09329f99f98da1fb326
   /* val JoinFile =   ClientsDS
      .join(transactionDS, Seq("id"))

    val ClientsTrans= spark.read
      .option("header", true)
      .option("inferSchema", true)
      .csv("./Join.csv/part-00000-4a2940c4-e6f8-4b5b-9228-6fbeff66a215-c000.csv")
*/



    // filtre Transction Credit
/*
    val CreditFiltre = ClientsTrans
      .filter((row) => row.getAs("DC").equals("C"))
      .groupBy("name")
      .agg(sum("amount").as("SumOfCreditPerClient"))
      //.show()
      .agg(sum("SumOfCreditPerClient").as("SumOfCreditTotal"))
    //.show()*/


    // filtre Transction Debit

   /* val DebitFiltre = ClientsTrans
      .filter((row) => row.getAs("DC").equals("D"))
      .agg(sum("amount").as("SumOfDebitPerClient"))
      //   .show()
      .agg(sum("SumOfDebitPerClient").as("SumOfDebitTotal"))*/
    // .show()



    /// " number of" transactions by clients*


/*
    val NbOfTransactionPerClient = ClientsTrans
      .groupBy("id")
      .agg(functions.count("id").as("NumbereOfTrans"))
      .orderBy(desc("NumbereOfTrans"))
    //.show()*/


    /*///le Max de transaction amount per clients

    val MaxAmountPerClient = ClientsTrans
      .groupBy("id")
      .agg(functions.max("amount").as("MaxAmount"))
      .orderBy(desc("MaxAmount"))
    // .show()


    ///le Min de transaction amount per clients

    val MinAmountPerClient = ClientsTrans
      .groupBy("id")
      .agg(functions.min("amount").as("MinAmount"))
      .orderBy(desc("MinAmount"))
    //  .show()



    // the sum of transactions
    val SumTransaction = ClientsTrans
      .groupBy("id")
      .agg(functions.sum("amount").as("SumOfTransaction"))
      .agg(sum("SumOfTransaction").as("SumOfTotalTransaction (Euros) "))
      .show()


*/







//}

