from faker import Faker
import random 
import numpy as np



fake=Faker()


def clientsgeneration():
	file = open("client.csv","w")
	file.write("clientId,name,birthday,address")
	file.write("\n")

	for _ in range(40):
		file.write(str(random.randint(1,40))+",")
		file.write(str(fake.name())+",")
		file.write(str(fake.day_of_month())+"-"+str(random.randint(1,12))+"-"+str(fake.year())+",")		
		file.write(str(fake.street_address()))
		file.write("\n")
	file.close()


def writinginfile(file,nboftransaction):
	idclient = str(random.randint(1,40))
	id = str(random.randint(1,nboftransaction-1))
	accountid = str(random.randint(13993,20994093)) 
	debitcredit = str(random.choice(['debit','credit']))
	amount = str(random.randint(1,1000))
	transactiondate = str(fake.day_of_month())+"-"+str(random.randint(1,12))+"-"+str(fake.year())
	file.write(id+","+amount+","+accountid+","+debitcredit+","+idclient+","+transactiondate+"\n")

def law(x,y,law,numberofclients):
	if (law == "normal"):
		file = open("transactionnormale.csv","w")
		transaction = np.random.normal(x,y)
		transaction = abs(transaction)
	elif (law == "uniform"):
		file = open("transactionuniforme.csv","w")
		transaction = np.random.uniform(x,y)

	nboftransactions = int(round(transaction))-1
	file.write("id,amount,accountId,typeDebitOrCredit,clientId,transactionDate\n")
	for i in range(numberofclients):	
		for _ in range(nboftransactions):
			writinginfile(file,nboftransactions)
	file.close()


clientsgeneration()
law(2,50,"normal",50)
law(10,80,"uniform",20)