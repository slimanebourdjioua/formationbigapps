package com.bigapps.common.models

case class Transaction(id: Int,amount: Int, accountId: Int,typeDebitOrCredit: String,clientId: Int,transactionDate:String)