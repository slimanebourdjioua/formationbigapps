package com.bigapps.common.models

import java.lang._


case class ClientsTransactions (client: Client ,transactions : Array[Transaction]) {

  def print_clients_transactions(): Unit ={
    println(this.client.name)
    this.transactions.foreach( x => println(x))
  }

  def equals(clientsTransactions: ClientsTransactions): Boolean= {
    var x = 0
    if (this.client.equals(clientsTransactions.client)){
      var equal = true
      for (t <- this.transactions) {
        if (t.equals(clientsTransactions.transactions.apply(x)) && equal){
          x+=1
          equal = true
        }
        else equal = false

      }
      if( equal == true) true
      else false
    }
    else false
  }


}