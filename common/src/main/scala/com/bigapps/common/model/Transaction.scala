package com.bigapps.common.model

case class Transaction(transactionId: Int,amount: Int,accountId: Int,typeDebitOrCredit: String,clientId: Int,transactionDate:String)
