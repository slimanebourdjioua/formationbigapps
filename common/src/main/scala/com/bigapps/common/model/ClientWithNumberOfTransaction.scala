package com.bigapps.common.model

case class ClientWithNumberOfTransaction (clientId: Int,name: String, birthday: String,address:String,numberOfTransaction : Long)
