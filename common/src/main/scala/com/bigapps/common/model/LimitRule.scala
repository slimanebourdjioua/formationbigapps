package com.bigapps.common.model

case class LimitRule(limitTotal: Int, limitByCategory: Map[String, Int]) {


  require {
    var int: Int = 0
    limitByCategory.values.foreach(v => {

      int += v
    })
    int <= limitTotal
  }


}
